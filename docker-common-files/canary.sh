#!/bin/bash

# This script is spawned via init, thus will not have docker variables passed
# to it by default....
# But we can still extract the ones we want
ADMIN_EMAIL=$( xargs -0 -L1 -a /proc/1/environ |grep ADMIN_EMAIL | cut -d= -f2 )

function do_admin_email {
  SUBJECT=$1
  BODY=$2

  MAILTMP=`/bin/mktemp /tmp/$DIST$$.XXXXXXXX`
  echo -e "\nDear admins,\n" >> $MAILTMP
  # ensure shell globbing doesn't mess up our email
  set -f
  echo -e $BODY >> $MAILTMP
  set +f
  echo -e "\n---\nBest regards,\nCERN Linux Droid\n(on behalf of the friendly humans of Linux Support)" >> $MAILTMP

  swaks --to $ADMIN_EMAIL --from linux.support@cern.ch --body $MAILTMP --header "Subject: $SUBJECT" --header "Reply-To: noreply.Linux.Support@cern.ch"
  rm -f $MAILTMP
}

# We need this as we didn't install via a kickstart. anaconda-addon will complain otherwise
touch /root/anaconda-ks.cfg

# Ensure we are using the testing snapshot
dnf -y install cern-dnf-tool
cern-dnf-tool -t

# Get snapshot as YYYMMDD
RELEASE=`cat /etc/yum/vars/cern*`
SNAPSHOT_DATE=`curl --silent http://linuxsoft.cern.ch/cern/alma/$RELEASE/snapshotdate.txt`

TITLE="cern-anaconda-addon-canary ($RELEASE)"

if [ -z $SNAPSHOT_DATE ]; then
  do_admin_email "$TITLE: FAIL" "The call to linuxsoft to determine the snapshot date failed"
fi

# Setup a graphical user environment (using the latest packages)
dnf -y install initial-setup-gui cern-anaconda-addon

if [ $? -ne 0 ]; then
  do_admin_email "$TITLE: FAIL" "The installation of initial-setup-gui/cern-anaconda-addon failed"
fi

# Spawn a virtual frame buffer

/usr/bin/Xvfb :1 &
sleep 1

# Launch initial-setup-graphical (presumabily with the CERN addon)

DISPLAY=:1 /usr/libexec/initial-setup/initial-setup-graphical &
sleep 10

# When launching initial-setup-graphical in the way, the window size is not full screen by default
# Let's fix that

WINDOW_ID=`DISPLAY=:1 xdotool search --onlyvisible -class initial-setup-graphical`
DISPLAY=:1 xdotool windowmove $WINDOW_ID 0 0
DISPLAY=:1 xdotool windowsize $WINDOW_ID 1280 720

# Take a screenshot, convert to jpg and then search for "CERN CUSTOMIZATIONS" (proof that the CERN addon loaded)

DISPLAY=:1 import -window root /root/canary.jpg
tesseract /root/canary.jpg /root/canary-output
sleep 3

grep -q "CERN CUSTOMIZATIONS" /root/canary-output.txt
if [ $? -ne 0 ]; then
  do_admin_email "cern-anaconda-addon-canary ($RELEASE): FAIL" "cern-anaconda-addon appears to be broken with ${RELEASE} (${SNAPSHOT_DATE})"
fi
# kill the container (the only way that seems to work)
reboot -f
