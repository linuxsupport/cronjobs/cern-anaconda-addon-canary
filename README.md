# cern-anaconda-addon-canary

## Deprecated

**Deprecated as of 04.12.2024**

This functionality really only existed to test breaking changes in CentOS Stream. With RHEL/ALMA, the changes don't occur frequently

In addition, `cern-anaconda-addon` will not be used in EL10

## Historical information

This is a workflow to test if the [cern-anaconda-addon](https://gitlab.cern.ch/linuxsupport/rpms/cern-anaconda-addon) "works" with the latest snapshot.

Both Alma8 and Alma9 are supported through unique nomad jobs

The container runs `/sbin/init` (a requirement for `initial-setup-gui`) and a `canary` service is defined. The `canary.service` performs the following:

1. Configures the container to use the 'testing' release `cern-dnf-tool -t`
2. Installs `cern-anaconda-addon` (and `initial-setup-gui`)
3. Spawn a virtual frame buffer
4. Launches `initial-setup-graphical` inside the vfb
5. Takes a screenshot of the vfb
6. Uses ocr to detect if "CERN CUSTOMIZATIONS" is found
7. Emails admins if this is not the case

Notes:

The nomad definition of this job tweaks the `schedule` and `retry` attempts so that the container will only ever launch once. This has been done as it was not known how to have init/systemd exit with a return code of 0, and as such this job will always 'fail'.
